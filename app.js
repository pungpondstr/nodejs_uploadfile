const express = require('express');
const multer = require('multer');
const ejs = require('ejs');
const path = require('path');
const cors = require('cors');
const body = require('body-parser');

// Set The Storage Engine
const storage = multer.diskStorage({
  destination: './public/uploads/',
  filename: function(req, file, cb){
    cb(null,file.fieldname + '-' + Date.now() + path.extname(file.originalname));
  }
});

// Init Upload
const upload = multer({
  storage: storage,
  limits:{fileSize: 8096 * 8096},
  fileFilter: function(req, file, cb){
    checkFileType(file, cb);
  }
}).single('myImage');

// Check File Type
function checkFileType(file, cb){
  // Allowed ext
  const filetypes = /jpeg|jpg|png|gif/;
  // Check ext
  const extname = filetypes.test(path.extname(file.originalname).toLowerCase());
  // Check mime
  const mimetype = filetypes.test(file.mimetype);

  if(mimetype && extname){
    return cb(null,true);
  } else {
    cb('Error: Images Only!');
  }
}

// Init app
const app = express();

app.use(cors())
app.use(body())

// EJS
app.set('view engine', 'ejs');

// Public Folder
app.use(express.static('./public'));

app.get('/', (req, res) => res.json({
  msg: 'welcome back-end upload images'
}));

app.post('/upload', (req, res) => {
  upload(req, res, (err) => {
    res.json({
      msg: 'File Uploaded!',
      file: `uploads/${req.file.filename}`
    })
  });
});

const port = 5000;

app.listen(port, () => console.log(`Server started on port ${port}`));